describe('Test home access', function() {
  it('Visit the home page', function() {
    cy.visit('https://vitrine-fc258.firebaseapp.com/')
  })
})

describe('Test basket access', function() {
    it('Visit the basket page', function() {
      cy.visit('https://vitrine-fc258.firebaseapp.com/')

      cy.contains('Panier').click()

      cy.url().should('include', '/basket')
    })
  })

describe('Test add product to basket', function() {
    it('add a product to basket', function() {
      cy.visit('https://vitrine-fc258.firebaseapp.com/')

      cy.contains('Ajouter au panier').click()

      cy.contains('Panier').click()

      cy.contains('Total').should('be.visible')
    })
  })

describe('Test delete from basket', function() {
    it('delete a product from basket', function() {
      cy.visit('https://vitrine-fc258.firebaseapp.com/')

      cy.contains('Ajouter au panier').click()

      cy.contains('Panier').click()

      cy.contains('Supprimer un du panier').click()

      cy.contains('Total').should('not.be.visible')

    })
  })

describe('Test return home from basket', function() {
    it('should return on home page', function() {
      cy.visit('https://vitrine-fc258.firebaseapp.com/')

      cy.contains('Panier').click()

      cy.url().should('include', '/basket')

      cy.contains('Home').click()

      cy.url().should('include', '/')

    })
  })