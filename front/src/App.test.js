import React from 'react';
import { render } from '@testing-library/react';
import App from './App';
import Enzyme, { mount, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16'
import renderer from 'react-test-renderer';
const nock = require('nock');

Enzyme.configure({ adapter: new Adapter() });

test('renders successfully', () => {
    render(<App />);
    const element = document.getElementsByClassName("nav")[0];

    expect(element).toBeInTheDocument();
})

test('Products are successfully initialized', async (done) => {
    nock('https://vitrine-back.herokuapp.com')
        .persist()
        .get('/products')
        .reply(200, {
            products: [{
                rowid: 0,      
                name: 'Semoirs 750a',
                description: 'semoir',
                price: 800,
                img: '750aSemoirs.jpg' 
            }]
        },
        { 'access-control-allow-origin': '*' });
        
    const wrapper = shallow(<App />);

    const instance = wrapper.instance();
    await instance.componentDidMount();

    expect(wrapper.state('products')).toEqual([{
        rowid: 0,      
        name: 'Semoirs 750a',
        description: 'semoir',
        price: 800,
        img: '750aSemoirs.jpg' 
    }])

    done()
});

test('function addBasket adds product to basket', () => {
    const wrapper = mount(<App />);
    const instance = wrapper.instance();

    let basket = wrapper.state('basket');

    expect(basket).toEqual([])

    instance.addBasket(1);

    expect(basket[0]).toEqual(undefined)
    expect(basket[1]).toEqual(1)

    instance.addBasket(1);

    expect(basket[0]).toEqual(undefined)
    expect(basket[1]).toEqual(2)
})

test('function removeBasket removes product from basket', () => {
    const wrapper = mount(<App />);
    const instance = wrapper.instance();

    let basket = wrapper.state('basket');

    instance.addBasket(1);
    instance.addBasket(1);

    expect(basket[0]).toEqual(undefined)
    expect(basket[1]).toEqual(2)

    instance.removeBasket(1);

    expect(basket[0]).toEqual(undefined)
    expect(basket[1]).toEqual(1)

    instance.removeBasket(1);

    expect(basket[0]).toEqual(undefined)
    expect(basket[1]).toEqual(undefined)
})

test('nothing happens when removing a non existing product from the basket', () => {
    
    const wrapper = mount(<App />);
    const instance = wrapper.instance();

    let basket = wrapper.state('basket');

    instance.removeBasket(1);

    expect(basket[1]).toEqual(undefined)
})