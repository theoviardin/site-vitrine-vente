import React from 'react';
import '../App.css';
import '../Products.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import PropTypes from 'prop-types';

import Product from '../components/Product';

function Home(props) {
    return (
      <div className="App">
        <div className="Products">
          {props.products.map((product, key) => {
            return (
              <Product 
                {...product} 
                key={key} 
                addBasket={props.addBasket} 
                removeBasket={props.removeBasket} 
              />
            )
          })}
        </div>
      </div>
    );
}

Home.propTypes = {
  products: PropTypes.array,
  addBasket: PropTypes.func,
  removeBasket: PropTypes.func
}

export default Home;