import React from 'react';
import PropTypes from 'prop-types';

function Basket(props) {
    
    return (
        <div className="basket" style={{display: 'flex', flexWrap: 'wrap'}}>
            {props.basket.map((quantite, key) => {
                
                const product = props.products.filter(elem => elem.rowid === key)[0]
                return (
                    <div key={key} className="card" style={{flex: '1 0 21%', margin: 10, maxWidth: 300}}>
                        <div className="card-body">
                            <img src={"./img/" + product.img} className="card-img-top" alt="..." />
                            <h5 className="card-title">{product.name} <b>x{quantite}</b></h5>
                            <h6 className="card-subtitle mb-2 text-muted">Total : {product.price * quantite} €</h6>
                            <p className="card-text">{product.description}</p>
                            <button onClick={() => props.removeBasket(product.rowid)} className="btn btn-danger">Supprimer un du panier</button>
                        </div>
                    </div>
                );
            })}
        </div>
    );
}

Basket.propTypes = {
    basket: PropTypes.array,
    products: PropTypes.array,
    removeBasket: PropTypes.func
}

export default Basket;
