import React from 'react';
import { render } from '@testing-library/react';
import Home from './Home';
import renderer from 'react-test-renderer';

test('product list empty by default', () => {
  render(<Home products={[]} />);
  const productList = document.getElementsByClassName("Products")[0];
  expect(productList).toBeEmpty();
});

test('product list not empty', () => {
  render(<Home products={[{
      rowid: 1, 
      name: '6610',
      description: 'tracteur petit',
      price: 20000,
      img: '6610.jpg'
  }]} />);
  const productList = document.getElementsByClassName("Products")[0];
  expect(productList).not.toBeEmpty();
});

test('non-regression of Home', () => {

  const tree = renderer
      .create(<Home products={[{
        rowid: 1, 
        name: '6610',
        description: 'tracteur petit',
        price: 20000,
        img: '6610.jpg'
    }]}/>)
      .toJSON();

  expect(tree).toMatchSnapshot();
})