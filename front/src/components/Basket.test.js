import React from 'react';
import { render } from '@testing-library/react';
import Basket from './Basket';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import renderer from 'react-test-renderer';

Enzyme.configure({ adapter: new Adapter() });

test('Basket doesnt have a button supprimer du panier', () => {
    let localState = {
        basket: [],
        products: [{ 
            rowid: 0,      
            name: 'Semoirs 750a',
            description: 'semoir',
            price: 800,
            img: '750aSemoirs.jpg'         
        }]
    };

    render(<Basket basket={localState.basket} products={localState.products} />);
    let buttonElement = document.getElementsByClassName("btn btn-danger")[0];

    expect(buttonElement).toEqual(undefined);
});

test('Basket has a button supprimer du panier', () => {
    let localState = {
        basket: [],
        products: [{ 
            rowid: 0,      
            name: 'Semoirs 750a',
            description: 'semoir',
            price: 800,
            img: '750aSemoirs.jpg'         
        }]
    };

    let addBasket = (idProduct) => {
        
        let basket = localState.basket ;
    
        if (basket[idProduct]) {
            basket[idProduct]++;
        } else {
            basket[idProduct] = 1;
        }

        localState.basket = basket;
    }

    addBasket(0);

    render(<Basket basket={localState.basket} products={localState.products} />);
    let buttonElement = document.getElementsByClassName("btn btn-danger")[0];

    expect(buttonElement).toBeInTheDocument();
});



test('remove product from basket', () => {
    let localState = {
        basket: [],
        products: [{ 
            rowid: 0,      
            name: 'Semoirs 750a',
            description: 'semoir',
            price: 800,
            img: '750aSemoirs.jpg'         
        }]
    };

    let addBasket = (idProduct) => {   
        
        let basket = localState.basket ;

        if (basket[idProduct]) {
            basket[idProduct]++;
        } else {
            basket[idProduct] = 1;
        }

        localState.basket = basket;
    }

    let removeBasket = (idProduct) => {
        
        let basket = localState.basket ;

        if (basket[idProduct] !== undefined) {
            if (basket[idProduct] > 1) {
                basket[idProduct]--;
            } else {
                delete basket[idProduct];
            }

            localState.basket = basket;
        }
    }

    addBasket(0);    

    const wrapper = mount(<Basket basket={localState.basket} products={localState.products} removeBasket={removeBasket} />);  
    
    const buttonElement = wrapper.find("button");
    buttonElement.simulate("click");

    expect(localState.basket[0]).toEqual(undefined);
});

test('non-regression of Basket', () => {

    let localState = {
        basket: [],
        products: [{ 
            rowid: 0,      
            name: 'Semoirs 750a',
            description: 'semoir',
            price: 800,
            img: '750aSemoirs.jpg'         
        }]
    };

    const tree = renderer
        .create(<Basket basket={localState.basket} products={localState.products} />)
        .toJSON();

    expect(tree).toMatchSnapshot();
})