import React from 'react';
import { render } from '@testing-library/react';
import Product from './Product';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16'
import renderer from 'react-test-renderer';

Enzyme.configure({ adapter: new Adapter() });

test('product has a button ajouter au panier', () => {
  render(<Product />);
  const buttonElement = document.getElementsByClassName("btn btn-primary")[0];
  expect(buttonElement).toBeInTheDocument();
});

test('can had products to the basket', async () => {
    
    let localState = {
        basket: []
    };

    let addBasket = (idProduct) => {
        let basket = localState.basket;
        if (basket[idProduct]) {
            basket[idProduct]++;
        } else {
            basket[idProduct] = 1;
        }

        localState.basket = basket;
    }
    
    expect(localState.basket[1]).toEqual(undefined)

    const wrapper = mount(<Product rowid={1} addBasket={addBasket} />);  
    const buttonElement = wrapper.find("button");
    buttonElement.simulate("click");
    
    expect(localState.basket[1]).toEqual(1)
});

test('non-regression of Product', () => {

    const tree = renderer
        .create(<Product />)
        .toJSON();

    expect(tree).toMatchSnapshot();
})