import React from 'react';
import PropTypes from 'prop-types';

function Product(props) {
  
  return (
    <div className="card" style={{flex: '1 0 21%', margin: 10, maxWidth: 300}}>
      <div className="card-body">
        <img src={"./img/" + props.img} className="card-img-top" alt="..." />
        <h5 className="card-title">{props.name}</h5>    
        <h6 className="card-subtitle mb-2 text-muted">{props.price} €</h6>
        <p className="card-text">{props.description}</p>
        <button onClick={() => props.addBasket(props.rowid)} className="btn btn-primary">Ajouter au panier</button>
      </div>
    </div>
  );
}

Product.propTypes = {
  rowid: PropTypes.number,
  name: PropTypes.string,
  description: PropTypes.string,
  price: PropTypes.number,
  img: PropTypes.string,
  addBasket: PropTypes.func
}

export default Product;
