import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Home from './components/Home';
import Basket from './components/Basket';

import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

/**
 * The general application
 * @constructor
 */
class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      basket: [],
      products: []
    };

    this.addBasket = this.addBasket.bind(this);
    this.removeBasket = this.removeBasket.bind(this);
  }

  /**
   * Add an item to the basket
   * @param {number} idProduct
   */
  addBasket(idProduct) {
    let basket = this.state.basket;

    if (basket[idProduct]) {
      basket[idProduct]++;
    } else {
      basket[idProduct] = 1;
    }

    this.setState({
      basket
    });
  }

  /**
   * Remove an item from the basket
   * @param {number} idProduct 
   */
  removeBasket(idProduct) {
    let basket = this.state.basket;

    if (basket[idProduct] !== undefined) {
      if (basket[idProduct] > 1) {
        basket[idProduct]--;
      } else {
        delete basket[idProduct];
      }

      this.setState({
        basket
      });
    }
  }

  async componentDidMount() {
      try{
        const response = await fetch(`https://vitrine-back.herokuapp.com/products`)
        const json = await response.json();
        
        this.setState({products: json.products})
      }
      catch (err) {}
  }

  render() {
    return (
      <Router>
        <div>
          <nav>
            <ul className="nav nav-pills justify-content-end">
              <li className="nav-item">
                <Link className="nav-link" to="/">Maison</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/basket">Panier</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link disabled" to="">Fabrice Duchamp</Link>
              </li>
            </ul>
            <hr />
          </nav>
          <Switch>
            <Route path="/basket">
              <Basket
                basket={this.state.basket}
                products={this.state.products}
                addBasket={this.addBasket}
                removeBasket={this.removeBasket}
              />{' '}
            </Route>{' '}
            <Route path="/">
              <Home
                basket={this.state.basket}
                products={this.state.products}
                addBasket={this.addBasket}
                removeBasket={this.removeBasket}
              />{' '}
            </Route>{' '}
          </Switch>{' '}
        </div>{' '}
      </Router>
    );
  }
}

export default App;
