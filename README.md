This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Site vitrine pour vente de tracteur

Ce répertoire contient deux dossier, un pour le back et un pour le front

Le front a été effectué avec du React et le back avec du nodeJs

C'est donc un projet Fullstack JS


### Pré-requis

Dans un premier temps, il faut installer  [npm](https://www.npmjs.com/get-npm) et [nodejs](https://nodejs.org/en/download/).

### Installation

Une fois le projet récupéré, installer toutes les dépendances pour le back et le front:

Back : 
```
cd back
npm install
```

Front :
```
cd front
npm install
```

### Lancer

Une fois que tout est installé, faire la commande suivante dans les deux différents dossiers : 

```
npm start
```
