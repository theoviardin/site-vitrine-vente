# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.1.0](https://gitlab.com/theoviardin/site-vitrine-vente/compare/v1.0.5...v1.1.0) (2020-03-25)


### Features

* change deploy order ([c66bf75](https://gitlab.com/theoviardin/site-vitrine-vente/commit/c66bf755800f592048ff31a56d96c61919d30513))

### [1.0.6](https://gitlab.com/theoviardin/site-vitrine-vente/compare/v1.0.5...v1.0.6) (2020-03-25)

### [1.0.5](https://gitlab.com/theoviardin/site-vitrine-vente/compare/v1.0.4...v1.0.5) (2020-03-24)

### [1.0.4](https://gitlab.com/theoviardin/site-vitrine-vente/compare/v1.0.3...v1.0.4) (2020-03-24)

### [1.0.3](https://gitlab.com/theoviardin/site-vitrine-vente/compare/v1.0.2...v1.0.3) (2020-03-23)

### [1.0.2](https://gitlab.com/theoviardin/site-vitrine-vente/compare/v1.0.1...v1.0.2) (2020-03-23)

### 1.0.1 (2020-03-23)
