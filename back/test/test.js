const chai = require('chai');
const chaiHttp = require('chai-http');

chai.should()
chai.use(chaiHttp);

describe('Product', () => {
	//Test get route
	describe('GET /products', () => {
		//Get all posts
		it('should get all products record', (done) => {
			chai.request('http://localhost:5000').get('/products').end((err, res) => {
				res.should.have.status(200);
				res.body.products.should.be.an('array')
				res.body.products[0].should.be.an('object').that.has.all.keys('rowid', 'name', 'description', 'price', 'img')
				done();
			});
		});

		//Get a post by id
		it('should get a single products by id', (done) => {
			const id = '1';
			chai.request('http://localhost:5000').get(`/products/${id}`).end((err, res) => {
				res.should.have.status(200);
				res.body.product.should.be.an('object').that.has.all.keys('rowid', 'name', 'description', 'price', 'img')
				done();
			});
		});

		it('should not get a single products record', (done) => {
			const id = 36;
			chai.request('http://localhost:5000').get(`/${id}`).end((err, res) => {
				res.should.have.status(404);
				done();
			});
		});
	});
});

describe('Client', () => {
	//Test get route
	describe('GET /clients', () => {
		//Get all posts
		it('should get all clients record', (done) => {
			chai.request('http://localhost:5000').get('/clients').end((err, res) => {
				res.should.have.status(200);
				res.body.clients.should.be.an('array')
				res.body.clients[0].should.be.an('object').that.has.all.keys('rowid', 'name')
				done();
			});
		});

		//Get a post by id
		it('should get a single clients by id', (done) => {
			const id = '1';
			chai.request('http://localhost:5000').get(`/clients/${id}`).end((err, res) => {
				res.should.have.status(200);
				res.body.client.should.be.an('object').that.has.all.keys('rowid', 'name')
				done();
			});
		});
	});
});
