/** Express router providing index related routes
 * @module routers/index
 * @requires express
 */

 /**
 * express module
 * @type {object}
 * @const
 * @namespace indexRouter
 */
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const swaggerUi = require('swagger-ui-express')
const YAML = require('yamljs')
const swaggerDocument = YAML.load('./swagger.yaml')

//database
const sqlite = require('sqlite3').verbose()
const db = new sqlite.Database(':memory:');

const app = express()

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use(cors())

/**
 * Get all the products.
 * @name get/products
 * @function
 * @memberof module:routers/index~indexRouter
 * @inner
 * @param {string} path - Express path
 * @param {callback} middleware - Express middleware.
 */
app.get('/products', (req, res) => {
  db.all(`
    SELECT rowid, * 
    FROM products
  `,
  (err, rows) => {
    console.log('/products')
    
    if (err) {
      res.status(500).json({"error" : err})
    } else {
      res.status(200).json({"products" : rows})
    }
  })
})

/**
 * Get one product.
 * @name get/products/:product
 * @function
 * @memberof module:routers/index~indexRouter
 * @inner
 * @param {string} path - Express path
 * @param {callback} middleware - Express middleware.
 */
app.get('/products/:product', (req, res) => {
  const product = req.params.product;

  db.get(`
    SELECT rowid, * 
    FROM products
    WHERE rowid = ?
  `,
  [ product ],
  (err, row) => {
    console.log('/products/:product')
    
    if (err) {
      res.status(500).json({"error" : err})
    } else {
      res.status(200).json({"product" : row})
    }
  })
})

/**
 * Get one client.
 * @name get/clients
 * @function
 * @memberof module:routers/index~indexRouter
 * @inner
 * @param {string} path - Express path
 * @param {callback} middleware - Express middleware.
 */
app.get('/clients', (req, res) => {
  db.all(`
  SELECT rowid, * 
  FROM clients
  `,
  (err, rows) => {
    console.log('/clients')
    
    if (err) {
      res.status(500).json({"error" : err})
    } else {
      res.status(200).json({"clients" : rows})
    }
  })
})

/**
 * Get one client.
 * @name get/clients/:client
 * @function
 * @memberof module:routers/index~indexRouter
 * @inner
 * @param {string} path - Express path
 * @param {callback} middleware - Express middleware.
 */
app.get('/clients/:client', (req, res) => {
  const client = req.params.client;

  db.get(`
    SELECT rowid, * 
    FROM clients
    WHERE rowid = ?
  `,
  [ client ],
  (err, row) => {
    console.log('/clients/:client')
    
    if (err) {
      res.status(500).json({"error" : err})
    } else {
      res.status(200).json({"client" : row})
    }
  })
})

const PORT = process.env.PORT || 5000;
app.listen(PORT, function () {
  db.run(`
    CREATE TABLE products (
      name TEXT NOT NULL,
      description TEXT,
      price INTEGER NOT NULL,
      img TEXT
    )
  `,
  (err) => {
    console.log('CREATE products')
    console.log(err)

    db.run(`
      CREATE TABLE clients (
        name TEXT NOT NULL
      )
    `,
    (err) => {
      console.log('CREATE clients')
      console.log(err)
  
      db.run(`
        CREATE TABLE purchases (
          clients INTEGER NOT NULL,
          products INTEGER NOT NULL,
          quantity INTEGER NOT NULL,
          FOREIGN KEY(clients)
            REFERENCES clients(idrow)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION,
          FOREIGN KEY(products)
            REFERENCES products(idrow)
              ON DELETE NO ACTION
              ON UPDATE NO ACTION
        )
      `,
      (err) => {
        console.log('CREATE relation')
        console.log(err)
    
        db.run(`
          INSERT INTO clients values (
            ?
          )
        `,
        ['Fabrice Duchamp'],
        (err) => {
          console.log('INSERT')
          console.log(err)

          const products = [
            {
              name: 'Semoirs 750a',
              description: 'semoir',
              price: 800,
              img: '750aSemoirs.jpg'
            },
            {
              name: '5090R',
              description: 'tracteur moyen',
              price: 40000,
              img: '5090R.png'
            },
            {
              name: '5125R',
              description: 'tracteur moyen',
              price: 41000,
              img: '5125R.png'
            },
            {
              name: '6610',
              description: 'tracteur petit',
              price: 20000,
              img: '6610.jpg'
            },
            {
              name: 'M962i',
              description: 'moisonneuse',
              price: 1000000,
              img: 'M962i.png'
            },
            {
              name: 'Nova',
              description: 'moisonneuse',
              price: 150000,
              img: 'Nova.jpg'
            },
            {
              name: 'Planteuse legume',
              description: 'planteuse',
              price: 10000,
              img: 'planteuses.jpg'
            },
            {
              name: 'tsp',
              description: 'faucheuse',
              price: 1000,
              img: 'tsp.jpg'
            },
            {
              name: 'W330',
              description: 'moisonneuse',
              price: 200000,
              img: 'w330_ptc.jpg'
            },
            {
              name: 'W440',
              description: 'moisonneuse',
              price: 300000,
              img: 'w440.jpg'
            },
          ]

          const stmt = db.prepare(`
            INSERT INTO products values (?, ?, ?, ?)
          `)

          products.map((product) => {
            stmt.run(product.name, product.description, product.price, product.img)
          })
          console.log('INSERT')
          console.log(err)

        })
      })
    })
  })
})

